var webpack = require('webpack');
var path = require('path');

module.exports = {
	mode: "development",
	context: __dirname,
	entry: "./src/jsx/scripts.jsx",
	devServer: {
		contentBase: path.join(__dirname, 'src'),
		compress: true,
		hot: true,
		port: 8080
	},
	node: {
		fs: "empty",
		net: 'empty',
		tls: 'empty',
		dns: 'empty'
	},
	module: {
	  rules: [
		{
			test: /\.jsx$/,
			exclude: /node_modules/,
			loader: "babel-loader",
			query: {
			  presets: ['react'],
			  plugins: [/*'react-html-attrs', 'transform-decorators-legacy', */'transform-class-properties'],
			}
		},
		{
			test: /\.css$/,
			use: ['style-loader', 'css-loader']
		}

	  ]
	},
	output: {
		path: __dirname + "/src",
		filename: "scripts.min.js"
	},
	plugins: [

		new webpack.HotModuleReplacementPlugin()
	]
};

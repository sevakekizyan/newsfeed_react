import Config from '../js/config.js';
import Actions from '../js/actions.js';
import Dispatcher from '../dispatcher/dispatcher.js';
import Guardian from 'guardian-js';


class GuardianStorage {
	constructor() {
		this.currentPage = 0;
		this.maxPages = 100;
		this.latestArticleDate = null;
		this.guardian = new Guardian(Config.GUARDIAN_API_KAY, false);
	}

	formatDate(date) {
		return `${date.getFullYear()}-${(date.getMonth() + 1)}-${date.getDate()}`;
	}

	getNewData(){
		if(!this.latestArticleDate) return;

		let date = this.formatDate(this.latestArticleDate);
		this.guardian.content.search(undefined,
				{
					'from-date': date,
					//'use-date': 'last-modified',
					'show-fields': 'thumbnail'
				}).then((resp)=>{
					let body = resp.toJSON().body;
					let response = JSON.parse(body).response;
					let articles = response.results;
					if(articles.length) {

						articles.forEach((article) => {
							article.isNew = true;
						})

						this.setLatestArticleDate(articles);
						Dispatcher.dispatch({
							type: Actions.ACTION_NEW_ARTICLES,
							value: articles
						});
					}
				})
	}
	populateNextData(){
		if(this.currentPage < this.maxPages) {
			this.guardian.content.search(undefined,
				{
					page: this.currentPage+1,
					'show-fields': 'thumbnail'
				})
			.then((resp)=>{
				let body = resp.toJSON().body;
				let response = JSON.parse(body).response;
				this.currentPage = response.currentPage;
				this.maxPages = response.pages;
				let articles = response.results;
				this.setLatestArticleDate(articles);
				Dispatcher.dispatch({
					type: Actions.ACTION_NEXT_PAGE,
					value: articles
				});
			})
			.catch(function(error){
				console.log(error);
			});
		}

	}
	getContent(id){
		this.guardian.item.search(id,
				{
					'show-fields':'all'
				})
			.then((resp)=>{
				let body = resp.toJSON().body;
				let response = JSON.parse(body).response;

				Dispatcher.dispatch({
					type: Actions.ACTION_ARTICLE_CONTENT,
					value: response.content
				});
			})
			.catch(function(error){
				console.log(error);
			});

	}

	setLatestArticleDate(articles) {
		articles.forEach((article) => {
			const date = new Date(article.webPublicationDate);
			if(date > this.latestArticleDate) {
				this.latestArticleDate = date;
			}
		});

	}

	dispatcherHandler(action) {
		switch(action.type) {
			case Actions.ACTION_ARTICLE_POPULATE:
				this.populateNextData();
				break;
			case Actions.ACTION_ARTICLE_GET_CONTENT:
				this.getContent(action.value);
				break;
		}
	}
}

const guardianStorage = new GuardianStorage();
Dispatcher.register(guardianStorage.dispatcherHandler.bind(guardianStorage));
export default guardianStorage;

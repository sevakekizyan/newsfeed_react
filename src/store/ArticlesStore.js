import { EventEmitter } from "events";

import Actions from '../js/actions.js';
import Dispatcher from '../dispatcher/dispatcher.js';

class ArticlesStore extends EventEmitter {
	constructor() {
		super();
		this.articles = [];
		this.currentArticle = null;
	}

	getAllData() {
		return this.articles;
	}

	getCurrentArticle() {
		return this.currentArticle;
	}

	filterDublicates() {

	    this.articles = this.articles.filter((obj, pos, arr) => {
	        return arr.map(mapObj => mapObj['id']).indexOf(obj['id']) === pos;
	    });

	}

	dispatcherHandler(action) {
		switch(action.type) {
			case Actions.ACTION_NEXT_PAGE:
				this.articles = [...this.articles, ...action.value];
				this.filterDublicates();
				this.emit(Actions.ACTION_ARTICLE_STORE_CHANGE);
				break;
			case Actions.ACTION_ARTICLE_CONTENT:
				this.currentArticle = action.value;
				this.emit(Actions.ACTION_ARTICLE_STORE_ARTICLE);
				break;
			case Actions.ACTION_NEW_ARTICLES:
				this.articles = [...action.value,...this.articles];
				this.filterDublicates();
				this.emit(Actions.ACTION_ARTICLE_STORE_CHANGE);
				break;
		}
	}
}

const articleStore = new ArticlesStore();
Dispatcher.register(articleStore.dispatcherHandler.bind(articleStore));
export default articleStore;

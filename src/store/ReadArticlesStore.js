import { EventEmitter } from "events";

import Actions from '../js/actions.js';
import Dispatcher from '../dispatcher/dispatcher.js';

import ArticlesStore from './ArticlesStore.js';

class ReadArticlesStore extends  EventEmitter {
	constructor() {
		super();
		this.read = [];
		this.load();
		ArticlesStore.on(Actions.ACTION_ARTICLE_STORE_ARTICLE, ()=>{
			this.setRead(ArticlesStore.getCurrentArticle().id);
		});
	}

	load() {
		let items = localStorage.getItem('readItems');
		if(items){
			this.read = JSON.parse(items);
		}else{
			this.read = [];
		}

	}

	getReadArticles() {
		return this.read;
	}

	isRead(id) {
		return this.contains(id);
	}

	contains(id) {
		return this.read.includes(id);
	}

	setRead(id) {
		if(!this.contains(id)) {
			this.read.push(id);
			localStorage.setItem('readItems', JSON.stringify(this.read));
		}

	}

	dispatcherHandler(action) {
		switch(action.type) {
			case Actions.ACTION_READ_ARTICLE:
				this.setRead(action.value);
				break;
		}
	}

}


const readArticlesStore = new ReadArticlesStore();
Dispatcher.register(readArticlesStore.dispatcherHandler.bind(readArticlesStore));
export default readArticlesStore;

import { EventEmitter } from "events";

import Actions from '../js/actions.js';
import Dispatcher from '../dispatcher/dispatcher.js';

class PinnedArticlesStore extends  EventEmitter {
	constructor() {
		super();
		this.pinned = [];
		this.load()
	}

	load() {
		let items = localStorage.getItem('pinnedItems');
		if(items){
			this.pinned = JSON.parse(items);
		}else{
			this.pinned = [];
		}

	}

	getPinnedArticles() {
		return this.pinned;
	}

	contains(element) {
		for(let article of this.pinned){
			if(article.id == element.id) {
				return true;
			}
		}
		return false;
	}

	indexOf(element) {

		return this.pinned.findIndex((article) => {
			return article.id == element.id;
		})

	}

	pin(element) {
		if(!this.contains(element)) {
			this.pinned.push(element);
			localStorage.setItem('pinnedItems', JSON.stringify(this.pinned));
		}

	}

	unpin(element) {
		const ind = this.indexOf(element)
		if(ind >= 0) {
			this.pinned.splice(ind, 1);
			localStorage.setItem('pinnedItems', JSON.stringify(this.pinned));
			this.emit(Actions.ACTION_PINNED_ARTICLE_STORE_CHANGE);
		}
	}

	dispatcherHandler(action) {
		switch(action.type) {
			case Actions.ACTION_PIN_ARTICLE:
				this.pin(action.value);
				break;

			case Actions.ACTION_UNPIN_ARTICLE:
				this.unpin(action.value);
				break;
			case Actions.ACTION_TOGGLE_PIN_ARTICLE:
				if(this.contains(action.value)) {
					this.unpin(action.value);
				} else {
					this.pin(action.value);
				}
		}
	}
}

const pinnedArticlesStore = new PinnedArticlesStore();
Dispatcher.register(pinnedArticlesStore.dispatcherHandler.bind(pinnedArticlesStore));
export default pinnedArticlesStore;

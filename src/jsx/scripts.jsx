import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory, applyRouterMiddleware } from "react-router";
import { useScroll } from 'react-router-scroll';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import Layout from './modules/container/Layout.jsx';
import ArticleContent from './modules/container/ArticleContent.jsx';

const app = document.getElementById('app');
ReactDOM.render(
	<Router
		history={hashHistory}
		render={applyRouterMiddleware(useScroll())}
	>
    	<Route path="/" component={Layout}>

    	</Route>
    	<Route path="/article/:id" component={ArticleContent}/>

    </Router>

	, app);

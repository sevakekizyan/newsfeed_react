import React from 'react';

const Alert = (props) => {
	const {render} = props;
	return (
		<div className="alert alert-warning alert-dismissible fade show">
        	<button type="button" className="close" data-dismiss="alert">&times;</button>
        	{render}
        </div>
	)
}

export default Alert;

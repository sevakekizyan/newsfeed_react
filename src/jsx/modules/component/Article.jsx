import React from 'react';
import {Link} from 'react-router';

const Article = (props) => {

	let className = "col-md-4";
	if(props.isNew) {
		className += " highlight";
	}
	const {
		webUrl,
		imgUrl,
		pillarName,
		read,
		webPublicationDate,
		webTitle
	} = props;
	return (
		<div className={className}>
			<div className="post">
				<Link className="post-img" to={webUrl}><img src={imgUrl} alt=""/></Link>
				<div className="post-body">
					<div className="post-meta">
						<a className="post-category cat-2" href="javascript:void(0);">{pillarName}</a>
						{read && (<a className="post-category cat-1">Read</a>)}
						<span className="post-date">{webPublicationDate}</span>

					</div>
					<h3 className="post-title"><Link to={webUrl}>{webTitle}</Link></h3>
				</div>
			</div>
		</div>
	);

}
export default Article;

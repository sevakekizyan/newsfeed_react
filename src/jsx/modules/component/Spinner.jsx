import React from 'react';

const Spinner = (props) => {

	return (
		<div className="row"><i className="fa fa-spinner fa-spin mx-auto"></i></div>
	)

}
export default Spinner;

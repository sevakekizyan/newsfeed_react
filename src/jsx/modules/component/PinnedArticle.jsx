import React from 'react';
import {Link} from 'react-router';

const PinnedArticle = (props) => {

	const {id, url, title} = props;
	const removeHandler = ()=> {
		props.removeHandler(id);
	}

	return (

		<span>
			<Link to={url}>{title}</Link>
			<button type="button" onClick={removeHandler} className="close" >&times;</button>
		</span>


	)

}
export default PinnedArticle

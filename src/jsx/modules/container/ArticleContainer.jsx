import React from 'react';
import Article from '../component/Article.jsx';
import Alert from '../component/Alert.jsx';
import Spinner from '../component/Spinner.jsx';

import Actions from '../../../js/actions.js';

import ArticlesStore from '../../../store/ArticlesStore.js';
import ReadArticlesStore from '../../../store/ReadArticlesStore.js';

export default class ArticleContainer extends React.Component {
	constructor() {
		super();
		this.state = {articles: ArticlesStore.getAllData()};
	}

	componentWillMount() {
		ArticlesStore.on(Actions.ACTION_ARTICLE_STORE_CHANGE, this.loadData);
	}

	componentWillUnmount() {
		ArticlesStore.removeListener(Actions.ACTION_ARTICLE_STORE_CHANGE, this.loadData);
	}

	loadData = () => {

		this.setState({articles: ArticlesStore.getAllData()});
	}

	scrollTop() {
		window.scrollTo(0, 0);
	}

	render() {
		const articles = this.state.articles;
		let hasNew = false;

		const articleComponents = articles.map((article)=>{
			if(article.isNew) {
				hasNew = true;
			}
			const props = {
				key: article.id,
				webUrl: `article/${encodeURIComponent(article.id)}`,
				pillarName: article.pillarName,
				webTitle: article.webTitle,
				imgUrl: article.fields ? article.fields.thumbnail : '',
				read: ReadArticlesStore.isRead(article.id),
				isNew: article.isNew,
				webPublicationDate: (new Date(article.webPublicationDate)).toDateString()
			}
			return <Article {...props}/>
		})

		return(
			<div className="section">
			{hasNew && (<Alert render={(<span>There are new <a data-dismiss="alert" href="javascript:void(0);" onClick={this.scrollTop}>posts</a></span>)} />)}
				<div className="container">
					<div className="row">
						{articleComponents}
					</div>
					<Spinner/>
				</div>
			</div>
		)

	}
}

import React from 'react';
import Dispatcher from '../../../dispatcher/dispatcher.js';
import Parser from 'html-react-parser';

import Actions from '../../../js/actions.js';

import ArticlesStore from '../../../store/ArticlesStore.js';
import PinnedArticlesStore from '../../../store/PinnedArticlesStore.js';

import Spinner from '../component/Spinner.jsx';

export default class ArticleContent extends React.Component {

	constructor(props) {
		super(props);
		this.state = {article:null};
		Dispatcher.dispatch({type:Actions.ACTION_ARTICLE_GET_CONTENT, value:props.params.id});
	}

	togglePin = () => {
		Dispatcher.dispatch({type: Actions.ACTION_TOGGLE_PIN_ARTICLE, value:{
				title: this.state.article.webTitle,
				id: this.state.article.id
			}
		});
		this.loadArticle()
	}

	read() {
		Dispatcher.dispatch({type: Actions.ACTION_READ_ARTICLE, value: this.state.article.id});
	}

	componentWillMount() {
		ArticlesStore.on(Actions.ACTION_ARTICLE_STORE_ARTICLE, this.loadArticle);
	}

	loadArticle = () => {
		this.setState({article: ArticlesStore.getCurrentArticle()});
	}

	componentWillUnmount() {
		ArticlesStore.removeListener(Actions.ACTION_ARTICLE_STORE_ARTICLE, this.loadArticle);
	}

	render() {

		const article = this.state.article;
		if(!article) {
			return (
				<Spinner/>
			)
		}

		const date = (new Date(article.webPublicationDate)).toDateString();
		const pinClass = PinnedArticlesStore.contains(this.state.article) ? 'eraser' : 'star';
		const style = {
			backgroundImage: `url('${article.fields.thumbnail}')`
		}
		const {pillarName, webTitle} = article;

		return (
			<div>
				<header id="header">
					<div id="post-header" className="page-header">
						<div className="background-img" style={style}></div>
						<div className="container">
							<div className="row">
								<div className="col-md-10">
									<div className="post-meta">
										<a className="post-category cat-2" href="#">{pillarName}</a>
										<span className="post-date">{date}</span>
									</div>
									<h1>{webTitle}</h1>
								</div>
							</div>
						</div>
					</div>
				</header>
				<div className="section">
					<div className="container">
						<div className="row">
							<div className="col-md-8">
								<div className="section-row sticky-container">
									<div className="main-post" >
									{Parser(article.fields.body)}
									</div>
									<div className="post-shares sticky-shares" title="Pin">
										<a href="javascript:void(0);" onClick={this.togglePin} className="share-facebook"><i className={'fa fa-'+pinClass+' text-center'}></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

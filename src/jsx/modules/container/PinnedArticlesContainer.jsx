import React from 'react';
import Actions from '../../../js/actions.js';
import PinnedArticlesStore from '../../../store/PinnedArticlesStore.js';
import Dispatcher from '../../../dispatcher/dispatcher.js';

import PinnedArticle from '../component/PinnedArticle.jsx';

export default class PinnedArticlesContainer extends React.Component {
	constructor() {
		super();
		this.state={items: PinnedArticlesStore.getPinnedArticles()};
	}

	removeArticle = (id) => {
		Dispatcher.dispatch({type: Actions.ACTION_UNPIN_ARTICLE, value:{id}});
	}

	componentWillMount() {
		PinnedArticlesStore.on(Actions.ACTION_PINNED_ARTICLE_STORE_CHANGE, this.loadData);
	}

	componentWillUnmount() {
		PinnedArticlesStore.removeListener(Actions.ACTION_PINNED_ARTICLE_STORE_CHANGE, this.loadData);
	}

	loadData = () => {
		this.setState({items: PinnedArticlesStore.getPinnedArticles()});
	}

	render() {
		const items = this.state.items;
		const listItems = items.map((item)=>{
			let props = {
				key: item.id,
				id: item.id,
				url: `article/${encodeURIComponent(item.id)}`,
				title: item.title,
				removeHandler: this.removeArticle
			}
			return <PinnedArticle {...props}/>
		})
		return (
			<div className="scrollmenu container">
					{listItems}
			</div>
		)
	}
}

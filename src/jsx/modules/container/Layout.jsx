import React from 'react';

import ArticleContainer from './ArticleContainer.jsx';
import PinnedArticlesContainer from './PinnedArticlesContainer.jsx';

import Storage from '../../../storage/GuardianStorage.js';
import Dispatcher from '../../../dispatcher/dispatcher.js';

export default class Layout extends React.Component {

	componentDidMount() {
		Dispatcher.dispatch({type:'populate'});
		let scrollTimer = null;
		document.addEventListener('scroll', function(e) {
			clearTimeout(scrollTimer);

			scrollTimer = setTimeout(function(){
				let distance = document.body.scrollHeight - (document.body.scrollTop + window.innerHeight);

				if (distance < window.innerHeight/2) {
					Dispatcher.dispatch({type:'populate'});
				}
			},500)


		});
		setInterval(()=>{Storage.getNewData();}, 30000);
	}
	render() {
    	return (
    		<div>
				<PinnedArticlesContainer/>
				<ArticleContainer/>
			</div>
    	);
  	}
}
